# composite_component

This component makes composite videos from clips it gets from [`device_video_component`](https://gitlab.com/such-software/do-expensive-things-once/device-video-component).

# Running the project

Install dependencies: `POSTURE=development ./install-gems.sh`

Start Message DB: `docker-compose up`

Start the component: `script/start`

# Dependencies

This project requires [`ffmpeg`](https://ffmpeg.org/).

On a Mac you can install it with: `brew install ffmpeg`.

This project has only been tested on a make.  We can't vouch for its operation on Windows at this time.

# Tests

`./test.sh`

# Copyright

Copyright 2022 Such Software, LLC.

# License

MIT
