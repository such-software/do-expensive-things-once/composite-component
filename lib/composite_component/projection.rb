module CompositeComponent
  class Projection
    include EntityProjection
    include Messages::Events

    entity_name :composite

    apply Initiated do |initiated|
      SetAttributes.(composite, initiated, copy: [
        { :composite_id => :id },
        :device_id,
        :start_minute,
        :end_minute,
      ])

      composite.initiated = true
    end

    apply Retrieved do |retrieved|
      composite.id = retrieved.composite_id

      composite.retrieved(retrieved.start_minute)
      composite.stored_uri(retrieved.uri)
    end

    apply RetrieveSent do |retrieve_sent|
      composite.id = retrieve_sent.composite_id

      composite.retrieve_sent(retrieve_sent.start_minute)
    end

    apply Built do |built|
      composite.id = built.composite_id

      composite.built = true
    end
  end
end
