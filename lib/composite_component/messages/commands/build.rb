module CompositeComponent
  module Messages
    module Commands
      class Build
        include Messaging::Message

        attribute :composite_id, String
        attribute :device_id, String
        attribute :start_minute, String
        attribute :end_minute, String
      end
    end
  end
end
