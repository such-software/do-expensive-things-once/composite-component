module CompositeComponent
  module Messages
    module Events
      class RetrieveSent
        include Messaging::Message

        attribute :composite_id, String
        attribute :device_id, String
        attribute :start_minute, String
      end
    end
  end
end
