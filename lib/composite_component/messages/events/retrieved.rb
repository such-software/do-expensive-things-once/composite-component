module CompositeComponent
  module Messages
    module Events
      class Retrieved
        include Messaging::Message

        attribute :composite_id, String
        attribute :device_video_id, String
        attribute :device_id, String
        attribute :start_minute, String
        attribute :uri, String
      end
    end
  end
end
