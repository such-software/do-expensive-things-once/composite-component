module CompositeComponent
  class Stitch
    def self.configure(receiver)
      instance = build

      receiver.stitch = instance
    end

    def self.build
      new
    end

    def call(composite_id, uris)
      working_dir = File.join(Dir.pwd, "tmp", composite_id)
      input_file = File.join(working_dir, "input.txt")
      output_file = File.join(working_dir, "output.mp4")

      Dir.mkdir(working_dir) unless File.exists?(working_dir)

      File.open(input_file, "w") do |file|
        uris.each do |uri|
          file.puts("file '#{uri}'")
        end
      end

      `ffmpeg -y -f concat -safe 0 -i #{input_file} -c copy #{output_file}`

      output_file
    end

    module Substitute
      def self.build
        Stitch.new
      end


      class Stitch
        attr_reader :called_with
        attr_accessor :uri

        def call(composite_id, uris)
          @called_with = uris 

          uri
        end
      end
    end
  end
end
