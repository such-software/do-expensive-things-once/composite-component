module CompositeComponent
  module CompositeId
    def self.decompose(composite_id)
      MessageStore::ID.parse(composite_id)
    end
  end
end
