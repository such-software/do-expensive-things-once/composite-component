require 'time'
require 'json'

module CompositeComponent
  class Composite
    include Schema::DataStructure

    SECONDS_PER_MINUTE = 60
    TIME_STRING_RANGE = 0..18

    attribute :id, String
    attribute :device_id, String
    attribute :start_minute, String
    attribute :end_minute, String
    attribute :initiated, Boolean
    attribute :uri, String
    attribute :built, Boolean
    attribute :source_uris, Collection::Array, default: -> { Collection::Array.new(String) }
    attribute :retrieved_minutes, Hash, default: -> { {} }

    def initiated?
      !!initiated
    end

    # This has a weakness in that if an end_minute that is less than the
    # start_minute gets used, we'll never achieve our termination condition.
    # Beware of operationalizing this exact code.
    def start_minutes
      return [] if start_minute.nil?

      minutes = [start_minute]

      current = Time.parse(start_minute) + SECONDS_PER_MINUTE
      end_raw = Time.parse(end_minute)

      while current <= end_raw
        minutes.push(current.iso8601[TIME_STRING_RANGE])

        current += SECONDS_PER_MINUTE
      end

      minutes
    end

    def retrieve_sent?(start_minute)
      retrieved_minutes[start_minute] == :sent_retrieve ||
        retrieved?(start_minute)
    end

    def retrieve_sent(start_minute)
      unless (retrieved_minutes.has_key?(start_minute))
        retrieved_minutes[start_minute] = :sent_retrieve
      end
    end

    def retrieved(start_minute)
      retrieved_minutes[start_minute] = :retrieved
    end

    def retrieved?(start_minute)
      retrieved_minutes[start_minute] == :retrieved
    end

    def all_retrieved?
      return false if start_minutes.length == 0

      start_minutes.each do |minute|
        return false if !retrieved?(minute) 
      end
    end

    def built?
      !!built
    end

    def stored_uri?(source_uri)
      source_uris.content.include?(source_uri)
    end

    def stored_uri(source_uri)
      source_uris.add(source_uri)
    end

    def uris
      source_uris.content
    end

    module Transform
      def self.instance(raw_data)
        raw_data[:source_uris] = Collection.Array(raw_data[:source_uris])
        raw_data[:retrieved_minutes] = deserialize_retrieved_minutes(raw_data[:retrieved_minutes])

        Composite.build(raw_data)
      end

      def self.raw_data(instance)
        raw_data = instance.to_h

        raw_data[:source_uris] = instance.source_uris.content
        raw_data[:retrieved_minutes] = instance.retrieved_minutes.to_json

        raw_data
      end

      def self.deserialize_retrieved_minutes(retrieved_minutes)
        parsed_retrieved_minutes = JSON.parse(retrieved_minutes)

        parsed_retrieved_minutes.transform_values(&:to_sym)
      end
    end
  end
end
