module CompositeComponent
  module Start
    def self.call
      Consumers::Commands.start("composite:command")
      Consumers::Events.start("composite")
      Consumers::DeviceVideo::Events.start("deviceVideo", identifier: "composite", correlation: "composite")
    end
  end
end
