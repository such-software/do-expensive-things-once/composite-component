module CompositeComponent
  module Handlers
    class Events
      include Messaging::Handle
      include Messaging::StreamName
      include Log::Dependency
      include Messages::Events

      dependency :write, Messaging::Postgres::Write
      dependency :retrieve, ::DeviceVideo::Client::Retrieve
      dependency :store, Store
      dependency :stitch, Stitch

      def configure(session: nil)
        Messaging::Postgres::Write.configure(self, session:)
        ::DeviceVideo::Client::Retrieve.configure(self, session:)
        Store.configure(self, session:)
        Stitch.configure(self)
      end

      category :composite

      handle Initiated do |initiated|
        composite_id = initiated.composite_id
        device_id = initiated.device_id

        composite, version = store.fetch(composite_id, include: :version)

        # These aren't all written to the same stream, so we're fine with
        # not writing them as a batch.
        composite.start_minutes.each_with_index do |start_minute, index|
          if composite.retrieve_sent?(start_minute)
            logger.info(tag: :ignored) { "Clip already requested.  Clip ignored (Event: #{initiated.message_type}, ID: #{composite_id}, start_minute: #{start_minute})" }
            next
          end

          retrieve.(device_id:, start_minute:, previous_message: initiated)

          retrieve_sent = RetrieveSent.follow(
            initiated,
            exclude: [:start_minute,:end_minute]
          )

          retrieve_sent.start_minute = start_minute

          stream_name = stream_name(composite_id)

          expected_version = version + index

          write.(retrieve_sent, stream_name, expected_version:)
        end
      end

      handle Retrieved do |retrieved|
        composite_id = retrieved.composite_id

        composite, version = store.fetch(composite_id, include: :version)

        if composite.built?
          logger.info(tag: :ignored) { "Composite alread built.  Event ignored (Event: #{retrieved.message_type}, ID: #{composite_id})" }
          return
        end

        unless composite.all_retrieved?
          logger.info(tag: :ignored) { "Segments not all retrieved.  Event ignored (Event: #{retrieved.message_type}, ID: #{composite_id})" }
          return
        end

        uri = stitch.(composite_id, composite.uris)

        built = Built.follow(
          retrieved,
          exclude: [:device_video_id, :start_minute, :uri]
        )

        SetAttributes.(built, composite, copy: [
          :start_minute,
          :end_minute
        ])

        built.uri = uri

        stream_name = stream_name(composite_id)

        write.(built, stream_name, expected_version: version)
      end
    end
  end
end
