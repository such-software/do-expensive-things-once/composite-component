module CompositeComponent
  module Handlers
    class Commands
      include Messaging::Handle
      include Messaging::StreamName
      include Log::Dependency
      include Messages::Commands
      include Messages::Events

      dependency :write, Messaging::Postgres::Write
      dependency :store, Store

      def configure(session: nil)
        Messaging::Postgres::Write.configure(self, session:)
        Store.configure(self, session:)
      end

      category :composite

      handle Build do |build|
        composite_id = build.composite_id

        composite, version = store.fetch(composite_id, include: :version)

        if composite.initiated?
          logger.info(tag: :ignored) { "Composite already initiated.  Command ignored (Command: #{build.message_type}, ID: #{build.composite_id})" }
          return
        end

        initiated = Initiated.follow(build)

        stream_name = stream_name(composite_id)

        initiated.metadata.correlation_stream_name = stream_name

        write.(initiated, stream_name, expected_version: version)
      end
    end
  end
end
