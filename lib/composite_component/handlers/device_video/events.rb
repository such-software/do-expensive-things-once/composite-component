module CompositeComponent
  module Handlers
    module DeviceVideo
      class Events
        include Messaging::Handle
        include Messaging::StreamName
        include Log::Dependency
        include Messages::Events

        dependency :write, Messaging::Postgres::Write
        dependency :store, Store

        def configure(session: nil)
          Messaging::Postgres::Write.configure(self, session:)
          Store.configure(self, session:)
        end

        category :composite

        handle ::DeviceVideo::Client::Messages::Events::Retrieved do |device_video_retrieved|
          correlation_stream_name = device_video_retrieved.metadata.correlation_stream_name

          if correlation_stream_name.nil?
            raise Error.new "No correlation_stream_name given"
          end

          composite_id = Messaging::StreamName.get_id(correlation_stream_name)
          start_minute = device_video_retrieved.start_minute
          
          composite, version = store.fetch(composite_id, include: :version)

          if composite.retrieved?(start_minute)
            logger.info(tag: :ignored) { "Clip already retrieved.  Event ignored (Event: #{device_video_retrieved.message_type}, ID: #{composite_id})" }
            return
          end

          retrieved = Retrieved.follow(device_video_retrieved, exclude: :workflow)

          retrieved.composite_id = composite_id

          stream_name = stream_name(composite_id)

          write.(retrieved, stream_name, expected_version: version)
        end
      end
    end
  end
end
