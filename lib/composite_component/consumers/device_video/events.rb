module CompositeComponent
  module Consumers
    module DeviceVideo
      class Events
        include Consumer::Postgres

        handler Handlers::DeviceVideo::Events
      end
    end
  end
end
