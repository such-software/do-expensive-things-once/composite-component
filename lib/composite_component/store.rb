module CompositeComponent
  class Store
    include EntityStore

    category :composite
    entity Composite
    projection Projection
    reader MessageStore::Postgres::Read, batch_size: 1000

    snapshot EntitySnapshot::Postgres, interval: 2
  end
end
