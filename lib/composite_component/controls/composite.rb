module CompositeComponent
  module Controls
    module Composite
      def self.example
        PartiallyRequested.example
      end

      def self.id
        "#{device_id}+#{start_minute}+#{end_minute}"
      end

      def self.device_id
        ID.example
      end

      def self.start_minute
        "2022-10-22T00:00:00"
      end

      def self.end_minute
        "2022-10-22T00:02:00"
      end

      module New
        def self.example
          CompositeComponent::Composite.build
        end
      end

      module Initiated
        def self.example(composite_id: nil)
          composite = New.example

          composite.id = composite_id || Composite.id
          composite.initiated = true
          composite.start_minute = Composite.start_minute
          composite.end_minute = Composite.end_minute

          composite
        end
      end

      module PartiallyRequested
        def self.example(composite_id: nil)
          composite = Initiated.example(composite_id:)

          start_minutes = composite.start_minutes[0..1]

          composite.retrieve_sent(start_minutes[0])
          composite.retrieved(start_minutes[1])

          composite
        end
      end

      module PartiallyRetrieved
        def self.example(start_minute_or_minutes)
          start_minutes = Array(start_minute_or_minutes)

          composite = Initiated.example

          start_minutes.each do |start_minute|
            composite.retrieved(start_minute)
            composite.stored_uri(start_minute)
          end

          composite
        end
      end

      module Retrieved
        def self.example
          composite = Initiated.example

          composite.start_minutes.each do |start_minute|
            composite.retrieved(start_minute)
            composite.stored_uri(start_minute)
          end
           
          composite
        end
      end

      module Built
        def self.example(composite_id = nil)
          composite = Retrieved.example

          composite.id = composite_id || Composite.id
          composite.built = true

          composite
        end
      end
    end
  end
end
