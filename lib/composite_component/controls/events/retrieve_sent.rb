module CompositeComponent
  module Controls
    module Events
      module RetrieveSent
        def self.example
          retrieve_sent = Messages::Events::RetrieveSent.build

          retrieve_sent.composite_id = Composite.id
          retrieve_sent.device_id = DeviceVideo::Client::Controls::ID::DeviceVideo.device_id
          retrieve_sent.start_minute = Composite.start_minute

          retrieve_sent
        end
      end
    end
  end
end
