module CompositeComponent
  module Controls
    module Events
      module Retrieved
        def self.example
          retrieved = Messages::Events::Retrieved.build

          retrieved.composite_id = Composite.id
          retrieved.device_id = DeviceVideo::Client::Controls::ID::DeviceVideo.device_id
          retrieved.start_minute = Composite.start_minute
          retrieved.uri = Uri.example

          retrieved
        end
      end
    end
  end
end
