module CompositeComponent
  module Controls
    module Events
      module Built
        def self.example
          built = Messages::Events::Built.build

          built.composite_id = Composite.id

          built
        end
      end
    end
  end
end
