module CompositeComponent
  module Controls
    module Events
      module Initiated
        def self.example
          initiated = Messages::Events::Initiated.build

          initiated.composite_id = Controls::Composite.id
          initiated.device_id = DeviceVideo::Client::Controls::ID::DeviceVideo.device_id
          initiated.start_minute = DeviceVideo::Client::Controls::ID::DeviceVideo.start_minute
          initiated.end_minute = end_minute

          initiated
        end

        def self.end_minute
          "2022-10-22T00:02:00"
        end
      end
    end
  end
end
