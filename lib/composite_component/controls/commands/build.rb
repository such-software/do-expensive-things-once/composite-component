module CompositeComponent
  module Controls
    module Commands
      module Build
        def self.example(device_id: nil, start_minute: nil, end_minute: nil)
          build = Messages::Commands::Build.build

          build.device_id = device_id || DeviceVideo::Client::Controls::DeviceVideo.device_id
          build.start_minute = start_minute || DeviceVideo::Client::Controls::DeviceVideo.start_minute
          build.end_minute = end_minute || self.end_minute

          build.composite_id = "#{build.device_id}+#{build.start_minute}+#{build.end_minute}"

          build
        end

        def self.end_minute
          "2022-10-22T00:02:00"
        end
      end
    end
  end
end
