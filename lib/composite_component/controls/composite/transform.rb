module CompositeComponent
  module Controls
    module Composite
      module Transform
        module RawData
          def self.example
            {
              id: ID.example,
              device_id: Composite.device_id,
              start_minute: Composite.start_minute,
              end_minute: Composite.end_minute,
              initiated: false,
              uri: Uri.example,
              built: false,
              source_uris: [Uri.example],
              retrieved_minutes: '{ "2022-10-22T00:00:00": "sent_receive"}'
            }
          end
        end
      end
    end
  end
end

