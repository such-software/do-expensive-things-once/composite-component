require "eventide/postgres"
require "device_video/client"
require "collection"

require "composite_component/composite_id"

require "composite_component/messages/commands/build"

require "composite_component/messages/events/initiated"
require "composite_component/messages/events/retrieved"
require "composite_component/messages/events/built"
require "composite_component/messages/events/retrieve_sent"

require "composite_component/composite"
require "composite_component/projection"
require "composite_component/store"
require "composite_component/stitch"

require "composite_component/handlers/commands"
require "composite_component/handlers/events"
require "composite_component/handlers/device_video/events"

require "composite_component/consumers/commands"
require "composite_component/consumers/events"
require "composite_component/consumers/device_video/events"

require "composite_component/start"
