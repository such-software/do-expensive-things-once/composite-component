require_relative "../../automated_init"

context "Handle DeviceVideo Events" do
  context "Retrieved" do
    context "Expected version" do
      device_video_retrieved = DeviceVideo::Client::Controls::Events::Retrieved.example

      composite_id = Controls::Composite.id
      composite_stream_name = "composite-#{composite_id}"
      device_video_retrieved.metadata.correlation_stream_name = composite_stream_name

      composite = Controls::Composite::Initiated.example

      version = Controls::Version.example

      handler = Handlers::DeviceVideo::Events.new
      handler.store.add(composite_id, composite, version)

      handler.(device_video_retrieved)

      writer = handler.write

      retrieved = writer.one_message do |event|
        event.instance_of?(Messages::Events::Retrieved)
      end

      test "Wrote Retrieved event" do
        refute(retrieved.nil?)
      end

      test "Written with expected_version" do
        written_to_stream = writer.written?(retrieved) do |_, expected_version|
          expected_version == version
        end

        assert(written_to_stream)
      end
    end
  end
end
