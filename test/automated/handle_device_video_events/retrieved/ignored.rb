require_relative "../../automated_init"

context "Handle DeviceVideo Events" do
  context "Retrieved" do
    context "Ignored" do
      composite_id = Controls::Composite.id

      device_video_retrieved = DeviceVideo::Client::Controls::Events::Retrieved.example
      device_video_retrieved.metadata.correlation_stream_name = "someStream-#{composite_id}"

      start_minute = device_video_retrieved.start_minute or fail

      composite = Controls::Composite::PartiallyRetrieved.example(start_minute)

      handler = Handlers::DeviceVideo::Events.new

      handler.store.add(composite_id, composite)

      handler.(device_video_retrieved)

      writer = handler.write

      retrieved = writer.one_message do |event|
        event.instance_of?(Messages::Events::Retrieved)
      end

      test "No message written" do
        assert(retrieved.nil?)
      end
    end
  end
end
