require_relative "../../automated_init"

context "Handle Device Video Events" do
  context "Retrieved" do
    context "Retrieved" do
      device_video_retrieved = DeviceVideo::Client::Controls::Events::Retrieved.example

      device_id = device_video_retrieved.device_id or fail
      start_minute = device_video_retrieved.start_minute or fail
      uri = device_video_retrieved.uri or fail

      composite_id = Controls::Composite.id
      composite_stream_name = "composite-#{composite_id}"
      device_video_retrieved.metadata.correlation_stream_name = composite_stream_name

      handler = Handlers::DeviceVideo::Events.new

      handler.(device_video_retrieved)

      writer = handler.write

      retrieved = writer.one_message do |event|
        event.instance_of?(Messages::Events::Retrieved)
      end

      test "Wrote Retrieved event" do
        refute(retrieved.nil?)
      end

      test "Written to composite stream" do
        written_to_stream = writer.written?(retrieved) do |stream_name|
          stream_name == "composite-#{composite_id}"
        end

        assert(written_to_stream)
      end

      context "Attributes" do
        test "composite_id" do
          assert(retrieved.composite_id == composite_id)
        end

        test "device_id" do
          assert(retrieved.device_id == device_id)
        end

        test "start_minute" do
          assert(retrieved.start_minute == start_minute)
        end

        test "uri" do
          assert(retrieved.uri == uri)
        end
      end
    end
  end
end
