require_relative "../automated_init"

context "Projection" do
  context "Retrieved" do
    retrieved = Controls::Events::Retrieved.example

    composite_id = retrieved.composite_id or fail
    start_minute = retrieved.start_minute or fail
    uri = retrieved.uri or fail

    composite = Controls::Composite::New.example

    Projection.(composite, retrieved)

    test "composite_id" do
      assert(composite.id == composite_id)
    end

    test "Retrieved start_minute" do
      assert(composite.retrieved?(start_minute))
    end

    test "Stored URI" do
      assert(composite.stored_uri?(uri))
    end
  end
end
