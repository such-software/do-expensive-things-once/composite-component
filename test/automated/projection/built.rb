require_relative "../automated_init"

context "Projection" do
  context "Built" do
    built = Controls::Events::Built.example

    composite_id = built.composite_id or fail

    composite = Controls::Composite::Retrieved.example

    Projection.(composite, built)

    test "composite_id" do
      assert(composite.id == composite_id)
    end

    test "built?" do
      assert(composite.built?)
    end
  end
end
