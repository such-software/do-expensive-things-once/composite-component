require_relative "../automated_init"

context "Projection" do
  context "Initiated" do
    initiated = Controls::Events::Initiated.example

    composite_id = initiated.composite_id or fail
    device_id = initiated.device_id or fail
    start_minute = initiated.start_minute or fail
    end_minute = initiated.end_minute or fail

    composite = Controls::Composite::New.example

    Projection.(composite, initiated)

    test "composite_id" do
      assert(composite.id == composite_id)
    end

    test "device_id" do
      assert(composite.device_id == device_id)
    end

    test "start_minute" do
      assert(composite.start_minute == start_minute)
    end

    test "end_minute" do
      assert(composite.end_minute == end_minute)
    end

    test "initiated?" do
      assert(composite.initiated?)
    end
  end
end
