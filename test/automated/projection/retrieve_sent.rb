require_relative "../automated_init"

context "Projection" do
  context "RetrieveSent" do
    retrieve_sent = Controls::Events::RetrieveSent.example

    composite_id = retrieve_sent.composite_id or fail
    start_minute = retrieve_sent.start_minute or fail

    composite = Controls::Composite::Initiated.example(composite_id:)

    Projection.(composite, retrieve_sent)

    test "retrieve_sent start_minute" do
      assert(composite.retrieve_sent?(start_minute))
    end
  end
end
