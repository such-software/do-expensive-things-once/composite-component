require_relative "../../automated_init"

context "Handle commands" do
  context "Build" do
    context "Ignored" do
      build = Controls::Commands::Build.example

      composite_id = build.composite_id or fail

      composite = Controls::Composite::Initiated.example(composite_id:)

      handler = Handlers::Commands.new

      handler.store.add(composite_id, composite)

      handler.(build)

      writer = handler.write

      initiated = writer.one_message do |event|
        event.instance_of?(Messages::Events::Initiated)
      end

      test "No Initiated written" do
        assert(initiated.nil?)
      end
    end
  end
end

