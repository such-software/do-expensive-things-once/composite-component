require_relative "../../automated_init"

context "Handle Commands" do
  context "Build" do
    context "Initiated" do
      build = Controls::Commands::Build.example

      composite_id = build.composite_id or fail
      device_id = build.device_id or fail
      start_minute = build.start_minute or fail
      end_minute = build.end_minute or fail

      handle = Handlers::Commands.new

      handle.(build)

      writer = handle.write

      initiated = writer.one_message do |event|
        event.instance_of?(Messages::Events::Initiated)
      end

      test "Wrote an Initiated event" do
        refute(initiated.nil?)
      end

      test "Written to composite stream" do
        written_to_stream = writer.written?(initiated) do |stream_name|
          stream_name == "composite-#{device_id}+#{start_minute}+#{end_minute}"
        end

        assert(written_to_stream)
      end

      context "attributes" do
        test "composite_id" do
          assert(initiated.composite_id == composite_id)
        end

        test "device_id" do
          assert(initiated.device_id == device_id)
        end

        test "start_minute" do
          assert(initiated.start_minute == start_minute)
        end

        test "end_minute" do
          assert(initiated.end_minute == end_minute)
        end
      end
    end
  end
end
