require_relative "../../automated_init"

context "Handle commands" do
  context "Build" do
    context "Expected version" do
      build = Controls::Commands::Build.example

      composite_id = build.composite_id or fail

      composite = Controls::Composite::New.example

      version = Controls::Version.example

      handler = Handlers::Commands.new

      handler.store.add(composite_id, composite, version)

      handler.(build)

      writer = handler.write

      initiated = writer.one_message do |event|
        event.instance_of?(Messages::Events::Initiated)
      end

      test "Written with expected version" do
        written_to_stream = writer.written?(initiated) do |_, expected_version|
          expected_version == version
        end

        assert(written_to_stream)
      end
    end
  end
end
