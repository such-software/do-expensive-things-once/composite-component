require_relative "../../automated_init"

context "Composite" do
  context "Transform" do
    context "instance" do
      raw_data = Controls::Composite::Transform::RawData.example

      source_uris = raw_data[:source_uris].clone

      retrieved_minutes = Composite::Transform.deserialize_retrieved_minutes(raw_data[:retrieved_minutes])

      instance = Composite::Transform.instance(raw_data)

      test "source_uris" do
        assert(instance.source_uris.content == source_uris)
      end

      test "retrieved_minutes" do
        assert(instance.retrieved_minutes == retrieved_minutes)
      end
    end
  end
end
