require "json"

require_relative "../../automated_init"

context "Composite" do
  context "Transform" do
    context "raw_data" do
      composite = Controls::Composite.example

      raw_data = Composite::Transform.raw_data(composite)

      test "source_uris" do
        assert(raw_data[:source_uris] == composite.source_uris.content)
      end

      test "retrieved_minutes" do
        deserialized_retrieved_minutes = Composite::Transform.deserialize_retrieved_minutes(raw_data[:retrieved_minutes])
        assert(deserialized_retrieved_minutes == composite.retrieved_minutes)
      end
    end
  end
end
