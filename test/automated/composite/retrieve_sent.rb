require_relative "../automated_init"

context "Composite" do
  context "retrieve_sent" do
    start_minute = Controls::Composite.start_minute
    composite = Controls::Composite::Initiated.example

    refute(composite.retrieve_sent?(start_minute))

    composite.retrieve_sent(start_minute)

    test "It recorded that we've sent the Retrieve command" do
      assert(composite.retrieve_sent?(start_minute))
    end
  end
end
