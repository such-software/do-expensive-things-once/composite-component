require_relative "../automated_init"

context "Composit" do
  context "all_received?" do
    retrieved_start_minutes = [
      "2022-10-22T00:00:00",
      "2022-10-22T00:01:00"
    ]

    composite = Controls::Composite::PartiallyRetrieved.example(retrieved_start_minutes)

    refute(composite.all_retrieved?)

    composite.retrieved("2022-10-22T00:02:00")

    test "It is all_received?" do
      assert(composite.all_retrieved?)
    end
  end
end

