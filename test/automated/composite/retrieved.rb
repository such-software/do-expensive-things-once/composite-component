require_relative "../automated_init"

context "Composite" do
  context "rerieved" do
    start_minute = Controls::Composite.start_minute
    composite = Composite.new

    refute(composite.retrieved?(start_minute))

    composite.retrieved(start_minute)

    test "It is marked retrieved" do
      assert(composite.retrieved?(start_minute))
    end

    test "It is marked sent_retrieve" do
      assert(composite.retrieve_sent?(start_minute))
    end
  end
end
