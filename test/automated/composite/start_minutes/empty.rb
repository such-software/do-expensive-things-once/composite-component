require_relative "../../automated_init"

context "Composite" do
  context "start_minutes" do
    context "empty" do
      composite = Controls::Composite::New.example

      call_count = 0

      composite.start_minutes do |_|
        call_count += 1
      end

      test "Called 0 times" do
        assert(call_count == 0)
      end
    end
  end
end

