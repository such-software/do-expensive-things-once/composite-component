require_relative "../../automated_init"

context "Composite" do
  context "start_minutes" do
    context "start_minutes" do
      composite = Controls::Composite::Initiated.example

      calls = []

      composite.start_minutes.each do |minute|
        calls.push(minute)
      end

      test "Calls 0" do
        assert(calls[0] == "2022-10-22T00:00:00")
      end

      test "Calls 1" do
        assert(calls[1] == "2022-10-22T00:01:00")
      end

      test "Calls 2" do
        assert(calls[2] == "2022-10-22T00:02:00")
      end
    end
  end
end

