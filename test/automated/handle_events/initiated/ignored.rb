require_relative "../../automated_init"

context "Handle events" do
  context "Initiated" do
    context "Ignored" do
      initiated = Controls::Events::Initiated.example

      composite_id = initiated.composite_id or fail
      device_id = initiated.device_id or fail

      retrieve_client = DeviceVideo::Client::Retrieve.new

      # Composite control where at least one of the RetrieveSents
      # was already written
      composite = Controls::Composite::PartiallyRequested.example(composite_id:)

      handler = Handlers::Events.new
      handler.retrieve = retrieve_client
      handler.store.add(composite_id, composite)

      handler.(initiated)

      context "RetrieveSent" do
        writer = handler.write

        retrieve_sent = writer.one_message do |event|
          event.instance_of?(Messages::Events::RetrieveSent)
        end

        # In a normal "ignored" test, we're explicitly checking that NOTHING
        # was written.  But in this case, the first two clips are already
        # requested, but the third one is not.  So, we want to make sure that
        # third clip still gets its message in place.
        #
        # However, we're not going to check all the attributes we normally
        # check on a written message because the "retrieve" test checks that.
        test "One RetrieveSent message written" do
          refute(retrieve_sent.nil?)
        end
      end

      context "Retrieve" do
        client_writer = retrieve_client.write

        start_minute = "2022-10-22T00:00:00"
        device_video_id = "#{device_id}+#{start_minute}"
        command_stream_name = "deviceVideo:command-#{device_video_id}"

        retrieve = client_writer.one_message do |command, stream_name|
          command.instance_of?(DeviceVideo::Client::Messages::Commands::Retrieve)
        end

        test "retrieve was written" do
          refute(retrieve.nil?)
        end
      end
    end
  end
end

