require_relative "../../automated_init"

context "Handle events" do
  context "Initiated" do
    context "Expected Version" do
      initiated = Controls::Events::Initiated.example

      composite_id = initiated.composite_id or fail

      composite = Controls::Composite::Initiated.example(composite_id:)

      version = Controls::Version.example

      handler = Handlers::Events.new

      handler.store.add(composite_id, composite, version)

      handler.(initiated)

      writer = handler.write

      retrieve_sents = writer.message_writes do |message|
        message.instance_of?(Messages::Events::RetrieveSent)
      end

      test "Wrote three RetrieveSent events" do
        assert(retrieve_sents.length == 3)
      end

      retrieve_sents.each_with_index do |retrieve_sent, index|
        expected_version = version + index

        test "Written with expected_version" do
          written_to_stream = writer.written?(retrieve_sent) do |_, written_expected_version|
            written_expected_version == expected_version
          end

          assert(written_to_stream)
        end
      end
    end
  end
end
