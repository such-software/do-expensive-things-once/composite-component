require_relative "../../automated_init"

context "Handle events" do
  context "Initiated" do
    context "Retrieve" do
      initiated = Controls::Events::Initiated.example
      composite_stream_name = "composite-#{Controls::Composite.id}"
      initiated.metadata.correlation_stream_name = composite_stream_name

      retrieve_client = DeviceVideo::Client::Retrieve.new

      composite_id = initiated.composite_id or fail
      device_id = initiated.device_id or fail

      composite = Controls::Composite::Initiated.example(composite_id:)

      start_minutes = composite.start_minutes

      handler = Handlers::Events.new
      handler.retrieve = retrieve_client
      handler.store.add(composite_id, composite)

      handler.(initiated)

      context "RetrieveSent" do
        writer = handler.write

        retrieve_sents = writer.message_writes do |message|
          message.instance_of?(Messages::Events::RetrieveSent)
        end

        test "Wrote three RetrieveSent events" do
          assert(retrieve_sents.length == 3)
        end

        retrieve_sents.each_with_index do |retrieve_sent, index|
          test "Written to composite stream" do
            written_to_stream = writer.written?(retrieve_sent) do |stream_name|
              stream_name == composite_stream_name
            end

            assert(written_to_stream)
          end

          context "Attribute" do
            test "composite_id" do
              assert(retrieve_sent.composite_id == composite_id)
            end

            test "device_id" do
              assert(retrieve_sent.device_id == device_id)
            end

            test "start_minute" do
              assert(retrieve_sent.start_minute == start_minutes[index])
            end
          end
        end
      end

      context "retrieve_1" do
        client_writer = retrieve_client.write

        start_minute = "2022-10-22T00:00:00"
        device_video_id = "#{device_id}+#{start_minute}"
        command_stream_name = "deviceVideo:command-#{device_video_id}"

        retrieve = client_writer.one_message do |command, stream_name|
          command.instance_of?(DeviceVideo::Client::Messages::Commands::Retrieve) &&
            stream_name == command_stream_name
        end

        test "retrieve was written" do
          refute(retrieve.nil?)
        end

        context "Attributes" do
          test "device_video_id" do
            assert(retrieve.device_video_id == device_video_id)
          end

          test "device_id" do
            assert(retrieve.device_id == device_id)
          end

          test "start_minute" do
            assert(retrieve.start_minute == start_minute)
          end
        end

        context "metadata" do
          test "correlation_stream_name" do
            assert(retrieve.metadata.correlation_stream_name == initiated.metadata.correlation_stream_name)
          end
        end
      end
    end
  end
end
