require_relative "../../automated_init"

context "Handle events" do
  context "Retrieved" do
    context "Ignored" do
      retrieved = Controls::Events::Retrieved.example
      
      composite_id = retrieved.composite_id or fail

      composite = Controls::Composite::Built.example(composite_id)

      handler = Handlers::Events.new

      handler.store.add(composite_id, composite)

      handler.(retrieved)

      writer = handler.write

      built = writer.one_message do |event|
        event.instance_of?(Messages::Events::Built)
      end

      test "No message was written" do
        assert(built.nil?)
      end
    end
  end
end
