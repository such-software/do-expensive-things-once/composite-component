require_relative "../../automated_init"

context "Handle Events" do
  context "Retrieved" do
    context "Not ready" do
      # In this test, we get a Retieved, but our clips aren't all ready yet.

      retrieved = Controls::Events::Retrieved.example

      handler = Handlers::Events.new

      handler.(retrieved)

      writer = handler.write

      test "Nothing was written" do
        assert(writer.message_writes.length == 0)
      end
    end
  end
end
