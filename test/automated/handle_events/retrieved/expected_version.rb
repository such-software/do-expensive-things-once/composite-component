require_relative "../../automated_init"

context "Handle events" do
  context "Retrieved" do
    context "Expected version" do
      retrieved = Controls::Events::Retrieved.example

      composite_id = retrieved.composite_id or fail

      composite = Controls::Composite::Retrieved.example

      version = Controls::Version.example

      handler = Handlers::Events.new

      handler.store.add(composite_id, composite, version)

      handler.(retrieved)

      writer = handler.write

      built = writer.one_message do |event|
        event.instance_of?(Messages::Events::Built)
      end

      test "Wrote a Built event" do
        refute(built.nil?)
      end

      test "Written with expected version" do
        written_to_stream = writer.written?(built) do |_, expected_version|
          expected_version == version
        end

        assert(written_to_stream)
      end
    end
  end
end
