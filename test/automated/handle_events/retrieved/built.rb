require_relative "../../automated_init"

context "Handle events" do
  context "Retrieved" do
    context "Build" do
      retrieved = Controls::Events::Retrieved.example

      composite_id = retrieved.composite_id or fail
      device_id = retrieved.device_id or fail

      uri = Controls::Uri.example

      composite = Controls::Composite::Retrieved.example
      uris = composite.uris or fail
      !uris.empty? or fail

      start_minute = composite.start_minute or fail
      end_minute = composite.end_minute or fail

      handler = Handlers::Events.new

      handler.store.add(composite_id, composite)
      handler.stitch.uri = uri

      handler.(retrieved)

      context "Build message" do
        writer = handler.write

        built = writer.one_message do |event|
          event.instance_of?(Messages::Events::Built)
        end

        test "Wrote a Built event" do
          refute(built.nil?)
        end

        test "Written to composite stream" do
          written_to_stream = writer.written?(built) do |stream_name|
            stream_name == "composite-#{composite_id}"
          end

          assert(written_to_stream)
        end

        context "Attributes" do
          test "composite_id" do
            assert(built.composite_id == composite_id)
          end

          test "device_id" do
            assert(built.device_id == device_id)
          end

          test "start_minute" do
            assert(built.start_minute == start_minute)
          end

          test "end_minute" do
            assert(built.end_minute == end_minute)
          end

          test "uri" do
            assert(built.uri == uri)
          end
        end
      end

      context "Lilo" do
        stitch = handler.stitch

        test "Stitched with the clips" do
          assert(stitch.called_with == uris)
        end
      end
    end
  end
end
