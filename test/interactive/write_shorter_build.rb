require_relative './interactive_init'

start_minute = "2022-10-22T00:01:00"
build = Controls::Commands::Build.example(start_minute:)
composite_id = build.composite_id

write = Messaging::Postgres::Write.build

stream_name = "composite:command-#{composite_id}"

write.(build, stream_name)
