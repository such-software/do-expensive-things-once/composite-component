require_relative './interactive_init'

start_minute = ARGV[0]
end_minute = ARGV[1]

build = Controls::Commands::Build.example(start_minute:, end_minute:)
composite_id = build.composite_id

write = Messaging::Postgres::Write.build

stream_name = "composite:command-#{composite_id}"

write.(build, stream_name)
