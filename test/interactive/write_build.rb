require_relative './interactive_init'

build = Controls::Commands::Build.example
composite_id = build.composite_id

write = Messaging::Postgres::Write.build

stream_name = "composite:command-#{composite_id}"

write.(build, stream_name)
